'use strict';

module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Twig
    twigRender: {
      dev: {
        files : [
          {
            data: 'app/data/data.json',
            expand: true,
            cwd: 'app/templates',
            src: ['*.twig', '!_*.twig'],
            dest: 'build',
            ext: '.html'
          }
        ]
      }
    },

    // Uglify
    uglify: {
      build: {
        files: {
          'build/assets/js/app.min.js' : ['app/assets/js/app.js']
        }
      },
      dev: {
        options: {
          beautify: true,
          mangle: false,
          compress: false,
          preserveComments: 'all'
        },
        files: {
          'build/assets/js/app.min.js' : ['app/assets/js/app.js']
        }
      }
    },

    // Sass
    sass: {
      build: {
        options: {
          outputStyle: 'compressed'
        },
        files: {
          'build/assets/css/app.css' : 'app/assets/scss/app.scss'
        }
      },
      dev: {
        options: {
          outputStyle: 'expanded'
        },
        files: {
          'build/assets/css/app.css' : 'app/assets/scss/app.scss'
        }
      }
    },

    // Rebuild when files are changed
    watch: {
      js: {
        files: ['app/assets/js/*.js'],
        tasks: ['uglify:dev']
      },
      css: {
        files: ['app/assets/scss/**/*.scss'],
        tasks: ['sass:dev']
      },
      twig: {
        files: ['app/templates/*.twig'],
        tasks: ['twigRender']
      },
      options: {
        livereload: true,
      }
    },

    // Watch our node server for changes
    nodemon: {
      dev: {
        script: 'server.js'
      }
    },

    // Run watch and nodemon at the same time
    concurrent: {
      options: {
        logConcurrentOutput: true
      },
      tasks: ['nodemon', 'watch']
    }
  });

  // load the plugins
  grunt.loadNpmTasks('grunt-twig-render');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-concurrent');

  // register task(s)
  grunt.registerTask('default', ['uglify:dev','sass:dev','twigRender','concurrent']);
  grunt.registerTask('build', ['uglify:build','sass:build']);
};
